<table></table>

<table>
<tr valign="top"><td width="50%">

# Thèmes LaTeX pour les écoles d'ingénieur INSA

</td><td>

# LaTeX themes for INSA engineering schools

</td></tr><tr valign="top"><td width="50%">

## Diaporama
Le thèmes est defini dans `beamerthemeINSA.sty`.  
Le modèle d'utilisation est `INSA_beamer.ltx`.  
Les images nécessaires sont dans le dossier `graphics/`.  

### Polices de caractères
La charte graphique du groupe INSA suggère la police de caractère Source Serif, ainsi que League Spartan pour les titres.  

La première est disponible dans les distributions `texlive`, et je propose de l'accompagner de la police mathématique fournie avec l'option `ncf` du paquet `newtxmath`, basée sur ScholaX, proche de New Century Schoolbook.  

La seconde peut être téléchargée et installée par exemple avec l'outil `autoinst LeagueSpartan*.ttf` (`texhash` et `updmap-sys` seront peut-être nécessaires pour terminer l'installation) ; on peut alors l'utiliser en passant l'option `LeagueSpartan` au thème `INSA` ; sinon, Helvetica est utilisée en remplacement.

Aussi, une police sans sérif peut être préférable pour des présentations projetées à l'écran, et je propose de passer l'option `sans` au thème `INSA` pour utiliser Source Sans, accompagnée de Arev (variante de Bitstream Vera Sans) pour les mathématiques.  
Les lettres imitation tableau noirs utilisent la police Doublestroke sans sérif.  
Les lettres calligraphiques utilisent la police ITC Zapf Chancery, mise à l'échelle avec le paquet `mathalfa`.  

### Divers
Le paquet `babel` avec l'option `french` n'est pas chargé par défaut dans le thème, mais est inclus dans le modèle.  

Le thème met à disposition les commandes `\makeINSAsummary[<options>]{<title>}` et `\makeINSAsectiontitlepage`.
La première crée une diapositive avec la table des matières, détaillant seulement la section en cours ; `<options>` est passée à `\tableofcontents` et `<title>` est le titre de la diapositive. 
La seconde crée une diapositive avec le nom de la section en cours.  

</td><td>

## Beamer
Theme is defined in `beamerthemeINSA.sty`.  
Use template is `INSA_beamer.ltx`.  
Required graphics are in `graphics/`.  

### Typefaces
The identity and style guidde of INSA recommends Source Serif typeface, together with League Spartan for titles.  

The first one is available in the `texlive` distribution, and I propose to match it with mathematical fonts provided by the package `newtxmath` with the option `ncf`, based on ScholaX, close to New Century Schoolbook.

The second one can be downloaded and installed for instance with the tool `autoinst LeagueSpartan*.ttf` (`texhash` and `updmap-sys` might be necessary to finish the installation process); one can then use it by specifying option `LeagueSpartan` to the `INSA` theme; otherwise, Helvetica is used as a replacement.

Moreover, a sans serif font might be preferable for a presentation projected on screen, et I suggest to pass the option `sans` to the `INSA` theme in order to use Source Sans, accompanied by Arev (variant of Bitstream Vera Sans) for the mathematics.
Blackboard letters are set to sans-serif Doublestroke typeface.  
Calligraphic letters are set as Zapf Chancery and scaled with the `mathalfa` package.

### Misc
The `babel` package with option `french` is not loaded by default in the theme, but included in the template.  

The theme also make available commands `\makeINSAsummary[<options>]{<title>}` and `\makeINSAsectiontitlepage`.  
The first one inserts a slide with the table of contents, detailing only the current section; `<options>` is passed to `\tableofcontents` and `<title>` is the title of the slide.  
The second one inserts a slide with the name of the current section.   

</td></tr>

<tr>
<td width="50%"><img src="Beamer/screenshots/INSA_beamer_1.svg" width="100%"/></td>
<td width="50%"><img src="Beamer/screenshots/INSA_beamer_2.svg" width="100%"/></td>
</tr><tr>
<td width="50%"><img src="Beamer/screenshots/INSA_beamer_3.svg" width="100%"/></td>
<td width="50%"><img src="Beamer/screenshots/INSA_beamer_4.svg" width="100%"/></td>
</tr><tr>
<td width="50%"><img src="Beamer/screenshots/INSA_beamer_5.svg" width="100%"/></td>
<td width="50%"><img src="Beamer/screenshots/INSA_beamer_8.svg" width="100%"/></td>
</tr>

<tr valign="top"><td width="50%">

##  Manuels et devoirs
Le thèmes est defini dans `INSA.sty`.  
Les modèles d'utilisation sont `INSA_textbook.ltx` et `INSA_assignment.ltx`.  
Les images nécessaires sont dans le dossier `graphics/`.  

### Police de caractères
Le thème utilise Source Serif pour le texte principal et League Spartan pour les titres, cette dernière pouvant être remplacée par Arial ; cf. ci-dessus section Diaporama pour les details.  

### Divers
Le paquet `babel` avec l'option `french` n'est pas chargé par défaut dans le thème, mais est inclus dans le modèle.  

Le type de document (manuel ou devoir) est déterminé par la présence d'une macro `\chapter` (utiliser typiquement les classes LaTeX `book` pour un manuel et `article` pour un devoir).  

</td><td>

##  Textbooks and assignments
Themes are defined in `INSA.sty`.  
Usage templates are `INSA_textbook.ltx` and `INSA_assignment.ltx`.  
Required graphics are in `graphics/`.  

### Typefaces
Use Source Serif and League Spartan, the latter can be replaced by Arial (see above Beamer section for details).  

### Misc
The `babel` package with option `french` is not loaded by default in the theme, but included in the template.  

Type of document (textbook or assignment) is determined by the presence of a `\chapter` command (use typically the LaTeX classes `book` for textbook and `article` for assignment).  

</td></tr>

<tr>
<td width="50%"><img src="Textbook_assignment/screenshots/INSA_textbook_1.svg" width="100%"/></td>
<td width="50%"><img src="Textbook_assignment/screenshots/INSA_textbook_2.svg" width="100%"/></td>
</tr><tr>
<td width="50%"><img src="Textbook_assignment/screenshots/INSA_textbook_3.svg" width="100%"/></td>
<td width="50%"><img src="Textbook_assignment/screenshots/INSA_textbook_4.svg" width="100%"/></td>
</tr><tr>
<td width="50%"><img src="Textbook_assignment/screenshots/INSA_assignment_1.svg" width="100%"/></td>
<td width="50%"><img src="Textbook_assignment/screenshots/INSA_assignment_2.svg" width="100%"/></td>
</tr>

</table>
