\ProvidesPackage{INSA}[2024 template for INSA textbook and assignments]
% use book class (or any class defining the \chapter command) for textbooks
% use article class (or any class not defining the \chapter command) for exams
% Hugo Raguet 2024

\newif\if@LeagueSpartan
\@LeagueSpartanfalse
\DeclareOption{LeagueSpartan}{\@LeagueSpartantrue}

\ProcessOptions\relax

%% Fonts
% Roman
% Source Serif Pro, as per the INSA identity and style Guide Jul 2022
\usepackage{sourceserifpro} % LaTeX version, no math support

% Sans-serif
% Mostly for titles and subtitles;
% Arial or League Spartan as per the INSA identity and style Guide Jul 2022
\if@LeagueSpartan
    % League Spartan Medium bold
    % scale must be set before reading *LeagueSpartan-TLF.fd file;
    % scale value from MatchLowercase option of .sty generated by autoinst
    \newcommand*{\LeagueSpartan@scale}{1.13277}
    \renewcommand*{\sffamily}{%
        \fontfamily{LeagueSpartan-TLF}\fontseries{medium}\fontshape{n}%
        \selectfont%
    }
    % use semi-bold instead of bold
    % first, force loading of the *LeagueSpartan-TLF.fd file
    % \input{T1LeagueSpartan-TLF.fd} % this assumes a font encoding
    {\sffamily} % this is more generic
    \DeclareFontShape{T1}{LeagueSpartan-TLF}{b}{n}{
          <-> alias * LeagueSpartan-TLF/sb/n
    }{}
\else
    % Helvetica is close to Arial
    \usepackage[scaled=1]{helvet}
\fi

% Monotype
\usepackage[scale=1.08]{nimbusmono}

% Math
% ncf selects a font based on ScholaX (close to New Century Schoolbook);
% scale factor suggested by The LaTeX Font Catalogue
% varbb provides adapted blackboard bold ; no need to load amssymb
% amsthm conflicts on command \openbox, loads it before
\usepackage[ncf, scaled=1.075, varbb, amsthm]{newtxmath}

%%  some colors
\RequirePackage{xcolor}
\definecolor{INSAgray}{HTML}{5F6062}
\definecolor{INSAred}{HTML}{E42618}
\definecolor{INSAorange}{HTML}{F69F1D}
\definecolor{INSApink}{HTML}{F5ADAA}
\definecolor{INSAbeige}{HTML}{F8F0EC}
\definecolor{INSAlightblue}{HTML}{8DA6D6}
\definecolor{INSAdarkblue}{HTML}{3B4395}

%%%  INSA  %%%

\RequirePackage{fancyhdr}
\RequirePackage{graphicx}
\RequirePackage{sectsty}
\RequirePackage{titlesec}

\newcommand*{\@INSAcourse}{Un super cours X}
\newcommand*{\@INSAshortcourse}{Cours~X}
\newcommand*{\@INSAyear}{STPI XA 20YY--20ZZ}
\newcommand*{\@INSAtitle}{SUPER COURS X}
\newcommand*{\@INSAshorttitle}{le cours}
\newcommand*{\@INSAsubtitle}{Notes de cours}
\newcommand*{\@INSAlogo}{INSA_CVL_white.pdf}
\newcommand*{\@INSAbookcover}{livret_page_de_garde_A4_rouge.pdf}
\newcommand*{\@INSAauthors}{Hugo~\textsc{Raguet}}
\newcommand*{\@INSAheader}{\@INSAshortcourse\;: \@INSAshorttitle}

\newcommand*{\INSAcourse}[2][]{%
    \renewcommand*{\@INSAcourse}{#2}
    \ifx\relax#1\relax
        \renewcommand*{\@INSAshortcourse}{#2}
    \else
        \renewcommand*{\@INSAshortcourse}{#1}
    \fi
}
\newcommand*{\INSAyear}[1]{\renewcommand*{\@INSAyear}{#1}}
\newcommand*{\INSAtitle}[2][]{%
    \renewcommand*{\@INSAtitle}{#2}
    \ifx\relax#1\relax
        \renewcommand*{\@INSAshorttitle}{\MakeLowercase{#2}}
    \else
        \renewcommand*{\@INSAshorttitle}{#1}
    \fi
}
\newcommand*{\INSAsubtitle}[1]{\renewcommand*{\@INSAsubtitle}{#1}}
\newcommand*{\INSAlogo}[1]{\renewcommand*{\@INSAlogo}{#1}}
\newcommand*{\INSAauthors}[1]{\renewcommand*{\@INSAauthors}{#1}}
\newcommand*{\INSAheader}[1]{\renewcommand*{\@INSAheader}{#1}}

\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\setlength{\headheight}{1.5ex}
\lhead{\sffamily\mdseries INSA CVL}
\chead{\sffamily\mdseries \@INSAheader}
\rhead{\sffamily\mdseries \@INSAyear}
\lfoot{}\cfoot{\thepage}\rfoot{}
% suppress those headers for standard plain page style
\fancypagestyle{plain}{\lhead{}\chead{}\rhead{}}

\newcommand*{\makeINSAtitle}[1][1ex]{% optional argument is vertical spacing
    \thispagestyle{empty}
    \sbox0{\sffamily\large\mdseries\@INSAcourse}
    \sbox1{%
        \raisebox{\dimexpr\depth-\dp0}[\dimexpr\totalheight-\dp0][\dp0]{%
            \sffamily\large\mdseries
            \renewcommand*{\arraystretch}{0}
            \begin{tabular}{@{}c@{}}
                \@INSAyear\\[1ex] \@INSAcourse
            \end{tabular}
        }%
    }%
    \noindent\includegraphics[height=\ht1]{\@INSAlogo}\hfill\copy1

    \vspace{#1}
    \vspace{#1}

    \begin{center}
        \LARGE\sffamily\color{black}\@INSAtitle
        \vspace{#1}\par
        % Et tellement + !
        \newlength{\tellementpluslength}
        \setlength{\tellementpluslength}{.012\textwidth}
        \leavevmode\leaders\hbox{%
            \hskip2\tellementpluslength%
            \rule[\dimexpr\tellementpluslength-.25pt]%
                {\dimexpr\tellementpluslength-.25pt}{.5pt}%
            \rule[0pt]{.5pt}{2\tellementpluslength}%
            \rule[\dimexpr\tellementpluslength-.25pt]%
                {\dimexpr\tellementpluslength-.25pt}{.5pt}%
            \hskip2\tellementpluslength%
        }\hskip.7\textwidth\kern0pt
        \ifx\@INSAsubtitle\empty\else
            \vspace{#1}\par
            {\color{INSAred}\bfseries\@INSAsubtitle}
        \fi
        \ifx\@INSAauthors\empty\else
            \vspace{#1}\par
            {\Large\color{INSAgray}\@INSAauthors}
        \fi
    \end{center}
    \vspace{#1}
    \vspace{#1}
}

\ifdefined\chapter % fancy style for book version
    \newcommand*{\INSAbookcover}[1]{\renewcommand*{\@INSAbookcover}{#1}}
    % absolute mode of textpos behaves badly (e.g. with pstricks)
    % workaround is to use \newgeometry... \restoregeometry to get a reference
    % point at the top left corner of the page
    \RequirePackage{textpos} 
    \setlength{\TPHorizModule}{1cm}
    \setlength{\TPVertModule}{1cm}
    \RequirePackage{geometry} 

    \newcommand*{\makeINSAbooktitle}{%
        \thispagestyle{empty}
        \newgeometry{inner=0cm, outer=0cm, bottom=0cm, top=-\baselineskip}

        \begin{textblock}{0}(0,0)
            \noindent\includegraphics[height=\paperheight]{\@INSAbookcover}
        \end{textblock}

        \begin{textblock}{0}(2.55,3.69)
            \noindent\includegraphics[height=1.53cm]{\@INSAlogo}
        \end{textblock}

        \begin{textblock}{6.1}[0,1](2.55,9.88)
            \noindent\parbox[b]{\textwidth}{% \parbox for correct interline
                \sffamily\LARGE\bfseries\color{INSAbeige}\@INSAtitle
            }
        \end{textblock}

        \ifx\@INSAsubtitle\empty\else
        \begin{textblock}{6}[0,.5](2.55,11.73)
            {\sffamily\LARGE\bfseries\color{INSAbeige}\noindent\@INSAsubtitle}
        \end{textblock}
        \fi

        \begin{textblock}{7.42}[0,0](12.37,3.69)
            {\hfill\sffamily\Large\color{INSApink}\@INSAyear\hfill}
        \end{textblock}

        \ifx\@INSAauthors\empty\else
        \begin{textblock}{6}[0,.5](2.55,15.43)
            {\sffamily\Large\color{INSApink}\noindent\@INSAauthors}
        \end{textblock}
        \fi

        \null % see §2.2 textpos package
        \restoregeometry
    }

    \setcounter{page}{0} % do not count first page

    % typesetting the title page, must be set before \allsectionsfont below
    \titlespacing{\chapter}{0pt}{*0}{*2}

    \titleformat{\chapter}
        % Et tellement + !
        {\color{INSAred}\titlerule*{%
                    \rule[0pt]{.5pt}{2ex}%
                    \hskip.53cm}% space between loose rays in cover
                    % \hskip.2cm}% space between dense rays in cover
        \vspace{1ex}%
        \sffamily\LARGE\bfseries\color{black}}
        {\LARGE\textbf{\textcolor{INSAbeige}{\thechapter{\mdseries+}}}}{0pt}{}

    \newcommand*{\INSAchapter}{% star is passed to chapter
        \@ifstar{\INSAchapter@star}{\INSAchapter@nostar}%
    }
    \newcommand*{\INSAchapter@star}[2][]{%
        \chapter*{#2}
        \INSAchapter@header{#1}{#2}
        \addcontentsline{toc}{chapter}{#2}
    }
    \newcommand*{\INSAchapter@nostar}[2][]{%
        \chapter{#2}
        \INSAchapter@header{#1}{#2}
    }
    % optional argument is for short version for header, not table of contents
    \newcommand*{\INSAchapter@header}[2]{%
        \ifx\relax#1\relax
            \INSAheader{\@INSAshortcourse\;: \MakeLowercase{#2}}
        \else
            \INSAheader{\@INSAshortcourse\;: #1}
        \fi
    }

    % create a table of contents without dedicating a new chapter
    \usepackage{tocloft}
    \newlength{\tellementplustoclength}
    \newcommand*{\INSAtoc}{%
        % \cftsetpnumwidth{5em} % make room for a full byte in table of content
        % Et tellement + !
        \setlength{\tellementplustoclength}{.6ex}
        \renewcommand*{\cftdot}{
            \rule[\dimexpr.5ex-.25pt]%
                {\dimexpr\tellementplustoclength-.25pt}{.5pt}%
            \rule[\dimexpr.5ex-\tellementplustoclength]{.5pt}%
                {2\tellementplustoclength}%
            \rule[\dimexpr.5ex-.25pt]%
                {\dimexpr\tellementplustoclength-.25pt}{.5pt}%
            % .53cm is space between loose rays in cover
            \hskip\dimexpr.53cm-2\tellementplustoclength+.5pt%
        }
        \renewcommand*{\cftdotsep}{0}
        \@starttoc{toc}%
    }
    % do not append chapter number in section numbering
    \renewcommand*{\thesection}{\arabic{section}}

\else % exercises and page numbering for assignments

    \usepackage{amsthm}
    \newtheoremstyle{INSAexo}{1ex}{1ex}% space above % space below
        {}{}% body font % indent amount
        {\sffamily}{.}% head font % punctuation after head
        {.5em}{} % space after head

    \usepackage{enumitem}
    \setlist[enumerate,1]{label={\rm(\alph{*})}}
    \setlist[enumerate,2]{label={\rm(\roman{*})}}

    \usepackage{lastpage}
    \cfoot{}\rfoot{\thepage/\pageref{LastPage}}

\fi % ifdefined\chapter

\allsectionsfont{\sffamily}
