% Beamer theme for INSA graphical identity
% Hugo Raguet 2024

\newif\if@beamerINSAsans
\@beamerINSAsansfalse
\DeclareOption{sans}{\@beamerINSAsanstrue}

\newif\if@LeagueSpartan
\@LeagueSpartanfalse
\DeclareOption{LeagueSpartan}{\@LeagueSpartantrue}

\ProcessOptions\relax

%%% Fonts
\usefonttheme{professionalfonts} % turn off Beamer math replacements

%% Main text fonts
% INSA identity and style Guide Jul 2022 recommands source serif
% but beamer are better read with sans serif fonts; package option `sans`

\if@beamerINSAsans
    \RequirePackage{sourcesanspro}
    \RequirePackage{arevmath}
    \SetSymbolFont{largesymbols}{normal}{OMX}{iwona}{m}{n}
    % use double stroke sans serif, scaled
    \DeclareFontFamily{U}{moddsss}{}
    \DeclareFontShape{U}{moddsss}{m}{n}{
          <-10> s*[1.05] dsss8
          <10-12> s*[1.05] dsss10
          <12-> s*[1.05] dsss12
    }{}
    \SetMathAlphabet{\mathbb}{normal}{U}{moddsss}{m}{n}
    \RequirePackage[cal=zapfc, calscaled=1.3]{mathalfa}
\else
    \RequirePackage{sourceserifpro} % LaTeX version, no math support
    \usefonttheme{serif}
    % Math
    % ncf selects a font based on ScholaX (close to New Century Schoolbook);
    % scale factor suggested by The LaTeX Font Catalogue
    % varbb provides adapted blackboard bold ; no need to load amssymb
    % amsthm conflicts on command \openbox, loads it before
    \usepackage[ncf, scaled=1.075, varbb, amsthm]{newtxmath}
\fi

%% Title fonts
% Arial or League Spartan as per the INSA identity and style Guide Jul 2022
\if@LeagueSpartan
    % scale must be set before reading *LeagueSpartan-TLF.fd file
    \newcommand*{\LeagueSpartan@scale}{1.1}
    \newcommand*{\Title}{\fontfamily{LeagueSpartan-TLF}\fontseries{medium}\fontshape{n}\selectfont}
    \newcommand*{\TitleLight}{\fontfamily{LeagueSpartan-TLF}\fontseries{m}\fontshape{n}\selectfont}
    {\Title} % force loading of the *.fd file
    % use semi-bold instead of bold
    \DeclareFontShape{T1}{LeagueSpartan-TLF}{b}{n}{
          <-> alias * LeagueSpartan-TLF/sb/n
    }{}
\else % Helvetica is a great open source replacement
    % scale must be set before reading *phv.fd file
    \newcommand*{\Hv@scale}{.95}
    \newcommand*{\Title}{\fontfamily{phv}\fontseries{b}\fontshape{n}\selectfont}
    \newcommand*{\TitleLight}{\fontfamily{phv}\fontseries{m}\fontshape{n}\selectfont}
\fi

\setbeamerfont{title}{size=\LARGE, family=\Title}
\setbeamerfont{subtitle}{size=\Large, family=\TitleLight}
\setbeamerfont{frametitle}{size=\Large, family=\Title}
\setbeamerfont{framesubtitle}{size=\Large, family=\TitleLight}
\setbeamerfont{author}{size=\normalsize, family=\Title}
\setbeamerfont{seminar}{size=\scriptsize, family=\TitleLight}
\setbeamerfont{date}{size=\scriptsize, family=\TitleLight}
\setbeamerfont{footline}{size=\scriptsize, family=\TitleLight}
\setbeamerfont{section in toc}{size=\large, family=\Title}
\setbeamerfont{subsection in toc}{size=\large, family=\TitleLight}


% layout and graphics
\RequirePackage[T1]{fontenc} % seems necessary to get correct \textgreater
\RequirePackage{calc}
\RequirePackage{array}
% \RequirePackage[absolute]{textpos}
% absolute mode of textpos behaves badly (e.g. with pstricks)
% workaround is to use \vspace{-\beamer@frametopskip} vertically
% and remove \Gm@lmargin form horizontal coordinates
% to get a reference point at the top left corner of the page
\RequirePackage{textpos} 

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{headline}{}

% some lengths
\setbeamersize{sidebar width left=5mm, sidebar width right=5mm,
    text margin left=10mm, text margin right=10mm}

\newcommand{\insertseminar}{}
\newcommand{\insertshortseminar}{}

\newcommand{\seminar}[2][\insertseminar]{
    \renewcommand{\insertshortseminar}{#1}
    \renewcommand{\insertseminar}{#2}
}

% color theme
\definecolor{INSAgray}{HTML}{5F6062}
\definecolor{INSAred}{HTML}{E42618}
\definecolor{INSAorange}{HTML}{F69F1D}
\definecolor{INSApink}{HTML}{F5ADAA}
\definecolor{INSAbeige}{HTML}{F8F0EC}
\definecolor{INSAlightblue}{HTML}{8DA6D6}
\definecolor{INSAdarkblue}{HTML}{3B4395}

\setbeamercolor{title}{fg=black}
\setbeamercolor{author}{fg=black}
\setbeamercolor{frametitle}{fg=INSAred}
\setbeamercolor{framesubtitle}{fg=INSApink}
\setbeamercolor{normal text}{fg=black}
\setbeamercolor{footline}{fg=INSAgray}
\setbeamercolor{structure}{fg=INSAorange}
\setbeamercolor{alerted text}{fg=INSAred}

\setbeamercolor{block title}{fg=INSAorange}
\setbeamercolor{block body}{bg=INSAbeige}

\setbeamertemplate{background}{%
    \includegraphics[width=\paperwidth, height=\paperheight]{graphics/diaporama_page_fond_1609.pdf}%
}

\defbeamertemplate*{title page}{}[1][]{
    \addtocounter{framenumber}{-1}
    \vspace{-\beamer@frametopskip}
    \newlength{\beamerINSA@titlerightmargin}
    \setlength{\beamerINSA@titlerightmargin}{.07\paperwidth-\Gm@lmargin}
    \begin{textblock*}{0cm}(-\Gm@lmargin,0cm)
        \includegraphics[height=\paperheight]%
            {graphics/diaporama_page_de_garde_1609.pdf}
    \end{textblock*}
    \begin{textblock*}{0cm}(\beamerINSA@titlerightmargin, .07\paperwidth)
        \inserttitlegraphic%
    \end{textblock*}
    \ifx\insertsubtitle\@empty%
        \begin{textblock*}{.6\paperwidth}%
                (\beamerINSA@titlerightmargin, .45\paperheight)
            \begin{beamercolorbox}[left,#1]{title}
                \usebeamerfont{title}\color{INSAbeige}\inserttitle
            \end{beamercolorbox}%
        \end{textblock*}
    \else%
        \begin{textblock*}{.6\paperwidth}%
                (\beamerINSA@titlerightmargin, .39\paperheight)
            \begin{beamercolorbox}[left,#1]{title}
                \usebeamerfont{title}\color{INSAbeige}\inserttitle
            \end{beamercolorbox}%
        \end{textblock*}
        \begin{textblock*}{.6\paperwidth}%
                (\beamerINSA@titlerightmargin, .59\paperheight)
            \begin{beamercolorbox}[left,#1]{subtitle}
                \usebeamerfont{subtitle}\color{INSAbeige}\insertsubtitle
            \end{beamercolorbox}%
        \end{textblock*}
    \fi%
    \begin{textblock*}{.6\paperwidth}%
            (\beamerINSA@titlerightmargin, .75\paperheight)
        \begin{beamercolorbox}[left,#1]{author}
            \usebeamerfont{author}\insertauthor
        \end{beamercolorbox}%
    \end{textblock*}
    \begin{textblock*}{.3\paperwidth}[.5,.5]%
            (.3\paperwidth-\Gm@lmargin, .95\paperheight)
        \begin{beamercolorbox}[center, #1]{date}
            \usebeamerfont{date}\insertdate
        \end{beamercolorbox}%
    \end{textblock*}
    \begin{textblock*}{.4\paperwidth}[.5,.5]%
                (.7\paperwidth-\Gm@lmargin, .95\paperheight)
        \begin{beamercolorbox}[center,#1]{seminar}
            \usebeamerfont{date}\insertseminar
        \end{beamercolorbox}%
    \end{textblock*}
}


\newif\if@insertframenumber
\@insertframenumbertrue

\defbeamertemplate*{footline}{}[1][]{
    \begin{beamercolorbox}[dp=2ex, wd=\paperwidth, leftskip=-10mm, #1]{footline}%
        \usebeamerfont{footline}%
        \hfill \insertshortdate{} \hfill \insertshortseminar{} %
        \hfill \insertshorttitle{} \hfill \makebox[1.5em][r]{%
            \if@insertframenumber \insertframenumber \else\fi%
        }~~
    \end{beamercolorbox}
}

%% Itemize
% prevent size shrinks within subitems
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize subitem}{size=\normalsize}
\setbeamercolor{itemize item}{fg=normal text.fg}
\setbeamercolor{itemize subitem}{fg=INSAred}
\setbeamercolor{itemize subsubitem}{fg=normal text.fg}
\newcommand*{\INSAbulletsquare}{\rule[.3ex]{.6ex}{.6ex}}
\newcommand*{\INSAbulletplus}{%
    \rule[.5ex]{.5ex-.25pt}{.5pt}%
    \rule[.25pt]{.5pt}{1ex}%
    \rule[.5ex]{.5ex-.25pt}{.5pt}%
}
\setbeamertemplate{itemize item}{\INSAbulletsquare}
\setbeamertemplate{itemize subitem}{\INSAbulletplus}
\setbeamertemplate{itemize subsubitem}{\INSAbulletsquare}

% useful for item list "by hand"
\newcommand*{\itembullet}{{\leavevmode\usebeamertemplate*{itemize item}}\hspace*{\labelsep}}
\newcommand*{\itemsubbullet}{{\leavevmode\usebeamertemplate*{itemize subitem}}\hspace*{\labelsep}}

% make section title frame
\newcommand{\makeINSAsectiontitlepage}{%
    \begingroup
    \@insertframenumberfalse
    \setbeamertemplate{background}{%
        \includegraphics[width=\paperwidth, height=\paperheight]%
            {graphics/diaporama_page_section_1609.pdf}%
    }
    \begin{frame}
        \vspace{-\beamer@frametopskip}
        \begin{textblock*}{.7\textwidth}%
                (.1\textwidth-\Gm@lmargin, .7\textheight)
            \usebeamerfont{title}%
            {\usebeamercolor[fg]{structure}\thesection{\TitleLight +}}%
            {\insertsectionhead}%
        \end{textblock*}
    \end{frame}
    \endgroup
}

% make a summary of current section
\newcommand{\makeINSAsummary}[2][hideothersubsections]{%
    \begingroup
    \@insertframenumberfalse
    \setbeamertemplate{background}{%
        \includegraphics[width=\paperwidth, height=\paperheight]%
            {graphics/diaporama_page_section_1609.pdf}%
    }
    \begin{frame}{#2}
        \normalsize \tableofcontents[#1]
    \end{frame}
    \endgroup
    \addtocounter{framenumber}{-1} % do not count it as frame
}
